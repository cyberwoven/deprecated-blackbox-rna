﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class ProfileRead
    {

        public string Username { get; set; }
        public string DisplayFirstName { get; set; }
        public string DisplayLastName { get; set; }
        public string CompanyName { get; set; }
        public string PreferredEmail { get; set; }
        public bool Commission { get; set; }
        public string Role { get; set; }
        public bool Terminated { get; set; }
        public bool ConventionCredits { get; set; }

    }
}
;
