﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class ConventionProgress
    {

        public string NameID { get; set; }
        public string AgentName { get; set; }
        public int AgentCredits { get; set; }
        public int RequiredCredits { get; set; }
        public string Location { get; set; }
        public DateTime Date { get; set; }
    }
}
