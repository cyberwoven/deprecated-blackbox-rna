﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class ProfileWrite
    {

        public string DisplayFirstName { get; set; }
        public string DisplayLastName { get; set; }
        public string CompanyName { get; set; }
        public int Pin { get; set; }
        public string PreferredEmail { get; set; }
    }
}
