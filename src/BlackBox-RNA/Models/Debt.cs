﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class Debt
    {

        public double IndividualDebt { get; set; }
        public List<DownlineDebt> DownlineDebt { get; set; }
    }
}
