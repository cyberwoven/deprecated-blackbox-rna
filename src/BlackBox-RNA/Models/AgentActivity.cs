﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace BlackBox_RNA.Models
{
    public class AgentActivity
    {
        public int AgentID { get; set; }
        public double Commissions { get; set; }
        public double Debt { get; set; }
        public double DownlineDebt { get; set; }
        public double ThirteenMonth { get; set; }
        public int Placement { get; set; }
        public double Ntel { get; set; }
    }
}
