﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class DownlineBusiness
    {

        public string AgentId { get; set; }
        public int TotalCertificates { get; set; }
        public double TotalValue { get; set; }
    }
}
