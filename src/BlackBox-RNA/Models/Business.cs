﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class Business
    {

        public string InsuredName { get; set; }
        public int CertificateNumber { get; set; }
        public string CoverageType { get; set; }
        public double FaceAmount { get; set; }
        public double Premium { get; set; }
        public string PaymentStatus { get; set; }
        public  int AgentID { get; set; }
        public string AgentName { get; set; }
        public string MoreInformation { get; set; }
        
    }
}
