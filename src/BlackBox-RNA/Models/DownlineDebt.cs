﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBox_RNA.Models
{
    public class DownlineDebt
    {
        //Here we will put our object attributes for each return type. This should be pretty straightforward, and buildable from the Swagger yaml file. Will expect some changes along the way./ 

        public string AgentId { get; set; }
        public string Name { get; set; }
        public double TotalDebt { get; set; }
    }
}
