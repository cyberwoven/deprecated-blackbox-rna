﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackBox_RNA.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BlackBox_RNA.Controllers
{
    [Route("api/[controller]")]
    public class DownlineDebtController : Controller
    {
        // GET: api/DownlineDebt
        [HttpGet]
        public DownlineDebt Get()
        {
            // create a DownlineDebt and then return it
            DownlineDebt dd = new DownlineDebt();
            dd.AgentId = "C1000";
            dd.Name = "Watson Johnson";
            dd.TotalDebt = 500.00;
            return dd;
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public int Get(int id)
        //{
        //    return id;
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
