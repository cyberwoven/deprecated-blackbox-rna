﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackBox_RNA.Models;
using Microsoft.AspNetCore.Mvc;

namespace BlackBox_RNA.Controllers
{
    [Route("profiles")]
    public class ConventionProgressController : Controller
    {
        //GET api/profilewrite
        [HttpGet("{id}/conventionProgress")]
        public ConventionProgress Get(int id)
        {
            ConventionProgress cp = new ConventionProgress();
            cp.NameID = "328475";
            cp.AgentName = "Agent Joe";
            cp.AgentCredits = 1203;
            cp.RequiredCredits = 3200;
            cp.Location = "Atlanta, GA";
            cp.Date = DateTime.Today;
            return cp;
        }

      

        

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
