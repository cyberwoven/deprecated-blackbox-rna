﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackBox_RNA.Models;
using Microsoft.AspNetCore.Mvc;

namespace BlackBox_RNA.Controllers
{
    [Route("agents/")]
    public class DownlineBusinessController : Controller
    {
        // GET api/Downlinebusiness
        [HttpGet("{agentID}/business/{status}")]
        public object Get(int agentID, string status)
        {
            // business array and downlineBusiness array
            Business b = new Business();
            b.InsuredName = "John Doe";
            b.CertificateNumber = 13000;
            b.CoverageType = "Whole Life";
            b.FaceAmount = 3500;
            b.Premium = 85.00;
            b.PaymentStatus = "Late";
            b.AgentID = agentID;
            b.AgentName = "Austin Smith";
            b.MoreInformation = "Some more information could go here.";

            Business b2 = new Business();
            b2.InsuredName = "John Stevens";
            b2.CertificateNumber = 2500;
            b2.CoverageType = "Not WHole Life";
            b2.FaceAmount = 10000;
            b2.Premium = 200;
            b2.PaymentStatus = "On-Time";
            b2.AgentID = agentID + 100;
            b2.AgentName = "Rob Johnson";
            b2.MoreInformation = "This is the second Business";


            DownlineBusiness dlb = new DownlineBusiness();
            dlb.AgentId = "C1000";
            dlb.TotalCertificates = 1;
            dlb.TotalValue = 500.00;
            DownlineBusiness dlb2 = new DownlineBusiness();
            dlb2.AgentId = "C2000";
            dlb2.TotalCertificates = 2;
            dlb2.TotalValue = 1000.00;
            var downlineBusiness = new List<DownlineBusiness>() {dlb, dlb2};
            var business = new List<Business>() {b, b2};

            return new {business, downlineBusiness };
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return id.ToString();
        //}

        // POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
