﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackBox_RNA.Models;
using Microsoft.AspNetCore.Mvc;

namespace BlackBox_RNA.Controllers
{
    [Route("profiles")]
    public class ProfileReadController : Controller
    {
        //GET api/profilewrite
        //[HttpGet]
        //public ProfileRead Get()
        //{
        //    ProfileRead pr = new ProfileRead();
        //    pr.Username = "real.name@gmail.com";
        //    pr.DisplayFirstName = "Andrew";
        //    pr.DisplayLastName = "Smith";
        //    pr.CompanyName = "MyCompany";
        //    pr.PreferredEmail = "andrewSmith@myCompany.com";
        //    pr.Commission = true;
        //    pr.Role = "IMO";
        //    pr.Terminated = false;
        //    pr.ConventionCredits = true;
        //    return pr;
        //}



        // GET api/values/5
        [HttpGet("{id}")]
        public ProfileRead Get(int id)
        {
            ProfileRead pr = new ProfileRead();
            pr.Username = "real.name@gmail.com";
            pr.DisplayFirstName = "Andrew";
            pr.DisplayLastName = "Smith";
            pr.CompanyName = "MyCompany";
            pr.PreferredEmail = "andrewSmith@myCompany.com";
            pr.Commission = true;
            pr.Role = "IMO";
            pr.Terminated = false;
            pr.ConventionCredits = true;
            return pr;
        }

        

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
