﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackBox_RNA.Models;
using Microsoft.AspNetCore.Mvc;

namespace BlackBox_RNA.Controllers
{
    [Route("api/[controller]")]
    public class ProfileWriteController : Controller
    {
        //GET api/profilewrite
        [HttpGet]
        public ProfileWrite Get()
        {
            ProfileWrite pw = new ProfileWrite();
            pw.DisplayFirstName = "Andrew";
            pw.DisplayLastName = "Smith";
            pw.CompanyName = "MyCompany";
            pw.Pin = 500;
            pw.PreferredEmail = "andrewSmith@myCompany.com";
            return pw;
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return id.ToString();
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
