﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackBox_RNA.Models;
using Microsoft.AspNetCore.Mvc;

namespace BlackBox_RNA.Controllers
{
    [Route("agents")]
    public class DebtController : Controller
    {
        //GET api/AgentActivity
        [HttpGet("{agentId}/debt")]
        public Debt Get(string agentID)
        {
            Debt d = new Debt();
            d.IndividualDebt = 500.00;

            DownlineDebt dd = new DownlineDebt();
            dd.AgentId = agentID;
            dd.Name = "Joe Manager";
            dd.TotalDebt = 500.00;

            DownlineDebt dd2 = new DownlineDebt();
            dd2.AgentId = "1" + agentID;
            dd2.Name = "James White";
            dd2.TotalDebt = 1000;

            var downlineDebt = new List<DownlineDebt> { dd, dd2 };

            d.DownlineDebt = downlineDebt;
            return d;
        }

        // GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return id.ToString();
        //}

        // POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
